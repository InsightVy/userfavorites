﻿using Insight.Common.SqlDatabaseContext;
using Insight.UserFavorites.SqlEntities;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Insight.UserFavorites.DataAccess
{
    public interface IUserFavoritesRepository: IRepository
    {
        List<UserFavorite> GetUserFavorites(string user);
        List<UserFavorite> GetAllUserFavorites(string user);
        User GetUser(string userId);
        void MarkIsActive(bool isActive, string userId, int reportId);
        void AddFavorites(UserFavorite userFavorites);
        IQueryable<Report> GetReports(Expression<Func<Report, bool>> expression);
        public int GetUserFavoritesCount();
        IQueryable<ReportGroupMapping> GetReportGroupMapping(Expression<Func<ReportGroupMapping, bool>> expression);
        IQueryable<SecurityGroup> GetSubGroups(Expression<Func<SecurityGroup, bool>> expression);
    }
}