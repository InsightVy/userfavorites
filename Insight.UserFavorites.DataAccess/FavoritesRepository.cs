﻿using Insight.Common.SqlDatabaseContext;
using Insight.UserFavorites.SqlEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Insight.UserFavorites.DataAccess
{
    public class UserFavoritesRepository : InsightRepository, IUserFavoritesRepository
    {
        private readonly UserFavoritesDbContext insightDbContext;

        public UserFavoritesRepository(UserFavoritesDbContext insightDbContext) : base(insightDbContext)
        {

            this.insightDbContext = insightDbContext;
        }

        public void AddFavorites(UserFavorite userFavorites)
        {
            this.insightDbContext.UserFavorites.Add(userFavorites);
        }

        public void MarkIsActive(bool isActive, string userId, int reportId)
        {
            var UserFavorites = this.insightDbContext.UserFavorites.FirstOrDefault(x => x.UserId == userId && x.ReportId == reportId);
            UserFavorites.IsActive = isActive;
            UserFavorites.UpdatedBy = userId;
            UserFavorites.UpdatedDateTime = DateTime.UtcNow;
            this.insightDbContext.UserFavorites.Update(UserFavorites);
        }

        public List<UserFavorite> GetUserFavorites(string user)
        {
            var utcNow = DateTime.UtcNow;
            var result = this.insightDbContext.UserFavorites.Where(x => x.IsActive == true && x.UserId == user);
            return result.ToList();
        }

        public List<UserFavorite> GetAllUserFavorites(string user)
        {
            var utcNow = DateTime.UtcNow;
            var result = this.insightDbContext.UserFavorites.Where(x => x.UserId == user);
            return result.ToList();
        }

        public List<UserFavorite> GetAllFavorites()
        {
            var utcNow = DateTime.UtcNow;
            var result = this.insightDbContext.UserFavorites.OrderByDescending(x => x.Id);
            return result.ToList();
        }

        public int GetUserFavoritesCount()
        {
            var result = GetAllFavorites().First().Id;
            return result;
        }

        public IQueryable<Report> GetReports(Expression<Func<Report, bool>> expression)
        {
            return this.insightDbContext.ReportCatalog.Where(expression).AsNoTracking();
        }

        public User GetUser(string userId)
        {
            return this.insightDbContext.user.FirstOrDefault(x => x.user_id == userId);
        }

        public IQueryable<ReportGroupMapping> GetReportGroupMapping(Expression<Func<ReportGroupMapping, bool>> expression)
        {
            return this.insightDbContext.ReportGroupMapping.Where(expression).AsNoTracking();
        }
        public IQueryable<SecurityGroup> GetSubGroups(Expression<Func<SecurityGroup, bool>> expression)
        {
            return this.insightDbContext.Groups.Where(expression).AsNoTracking();
        }
    }
}
