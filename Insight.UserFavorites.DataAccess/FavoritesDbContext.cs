﻿using Insight.Common.SqlDatabaseContext;
using Insight.UserFavorites.SqlEntities;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace Insight.UserFavorites.DataAccess
{
    public class UserFavoritesDbContext: InsightDbContext
    {
        public UserFavoritesDbContext(IDbConnection connection) : base(connection)
        {
        }

        public DbSet<UserFavorite> UserFavorites { get; set; }

        public DbSet<User> user { get; set; }

        public DbSet<Report> ReportCatalog { get; set; }

        public DbSet<ReportGroupMapping> ReportGroupMapping { get; set; }
        public DbSet<SecurityGroup> Groups { get; set; }
    }
}
