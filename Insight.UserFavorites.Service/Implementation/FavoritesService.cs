﻿using Insight.Common.SqlDatabaseContext;
using Insight.UserFavorites.DataAccess;
using Insight.UserFavorites.SqlEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Vyaire.Common.Utility;
using System.Threading.Tasks;
using Insight.UserFavorites.Service.Models;
using Vyaire.Common;

namespace Insight.UserFavorites.Service
{
    public class UserFavoritesService : IUserFavoritesService
    {
        private readonly IUserFavoritesRepository repository;

        public UserFavoritesService(IUserFavoritesRepository repository)
        {
            this.repository = repository;
        }

        public QuickSightGroups GetUserFavorites(UserFavoritesGetRequestModel request)
        {
            Arch.Logger.Log("Inside method GetUserFavorites Service");
            var userId = request.UserId.ExtractUserId();
            var quickSightGroup = new QuickSightGroups();
            try
            {

                Arch.Logger.Log("Get User");

                var user = GetUser(userId);

                Arch.Logger.Log("End User");

                if (user == null)
                {
                    user = new User()
                    {
                        user_id = userId
                    };
                }
                Arch.Logger.Log("Querying SQL");

                var subGroupsTask = GetSubGroups(request.GroupId);
                var reportsTask = GetReports();
                var reportGroupMappingTask = GetReportGroupMapping();
                var reportsFromDB = reportsTask.Result;
                var userFavoritesFromDB = this.repository.GetUserFavorites(userId);
                
                //Get sub groups of given group
                var subGroupsIds = new List<int>();
                subGroupsIds.Add(request.GroupId);
                subGroupsIds.AddRange(subGroupsTask.Result.Where(x => x.ParentGroupId == request.GroupId).Select(x => x.Id).ToList());

                var groupReportsFromDB = reportGroupMappingTask.Result.Where(x => subGroupsIds.Contains(x.GroupId)).Select(x => x.ReportId).ToList();

                Arch.Logger.Log("End of SQL Data");

                using (var tran = this.repository.StartTransaction())
                {
                    quickSightGroup = new QuickSightGroups()
                    {
                        group_name = "Favorites",
                        group_id = "Favorites",
                        qkgroupspk_id = "Favorites",
                        s3iconurl = "",
                        user_id = user.user_id,
                        user_name = user.user_name,
                        quicksightDashboards = new List<QuickSightDashboards>()
                    };

                    foreach (var userFavorite in userFavoritesFromDB.Where(x => groupReportsFromDB.Contains(x.ReportId)))
                    {
                        var reportDataForFavorite = reportsFromDB.Where(x => x.Id == userFavorite.ReportId).FirstOrDefault();

                        QuickSightDashboards oqckSightDashboards = new QuickSightDashboards();
                        oqckSightDashboards.dashboardpk_id = reportDataForFavorite.Id.ToString();
                        oqckSightDashboards.dashboard_id = reportDataForFavorite.ExternalReportId;
                        oqckSightDashboards.dashboard_name = reportDataForFavorite.ReportDisplayName;
                        oqckSightDashboards.dashboard_url = reportDataForFavorite.ReportSampleUrl;
                        oqckSightDashboards.group_id = "";
                        oqckSightDashboards.dashboards3imagepath = reportDataForFavorite.ReportSnapshotImageUrl;
                        oqckSightDashboards.dashboards3filepath = reportDataForFavorite.ReportUrl;
                        oqckSightDashboards.qkgroupspk_id = request.GroupId.ToString();
                        oqckSightDashboards.user_id = user.user_id;
                        oqckSightDashboards.user_name = user.user_name;
                        oqckSightDashboards.ReportPlatform = reportDataForFavorite.ReportPlatform;
                        oqckSightDashboards.IsQuicksight = reportDataForFavorite.ReportPlatform == "QuickSight" ? "Y" : "N";
                        quickSightGroup.quicksightDashboards.Add(oqckSightDashboards);
                    }
                }
            }
            catch (Exception ex)
            {
                Arch.Logger.Log("Inner Exception on database repo GetUserDashboards(): " + ex.InnerException.ToString());
                Arch.Logger.Log("Stack Trace on database repo GetUserDashboards(): " + ex.StackTrace.ToString());
                Arch.Logger.Log("Source on database repo GetUserDashboards(): " + ex.Source.ToString());
            }

            return quickSightGroup;
        }

        public bool AddUserFavorites(UserFavoritesRequestModel userFavoritesRequest)
        {
            var userFavoritesFromDB = this.repository.GetAllUserFavorites(userFavoritesRequest.UserId);

            var existingFavorite = userFavoritesFromDB.Where(x => x.ReportId == userFavoritesRequest.ReportId);

            if (existingFavorite.Count() > 0)
            {
                MarkIsActive(true, userFavoritesRequest.UserId, userFavoritesRequest.ReportId);
            }
            else
            {
                var nextPkid = this.repository.GetUserFavoritesCount() + 1;
                var userFavorites = new UserFavorite
                {
                    Id = nextPkid,
                    ReportId = userFavoritesRequest.ReportId,
                    UserId = userFavoritesRequest.UserId,
                    IsActive = true,
                    CreatedBy = userFavoritesRequest.UserId.ExtractUserId(),
                    CreatedDateTime = DateTime.UtcNow
                };

                this.repository.AddFavorites(userFavorites);
                this.repository.CommitChanges();
            }
            return true;
        }
        public bool RemoveUserFavorites(UserFavoritesRequestModel userFavoritesRequest)
        {
            var userFavoritesFromDB = this.repository.GetUserFavorites(userFavoritesRequest.UserId);

            var existingFavorite = userFavoritesFromDB.Where(x => x.ReportId == userFavoritesRequest.ReportId);

            if (existingFavorite.Count() > 0)
            {
                MarkIsActive(false, userFavoritesRequest.UserId, userFavoritesRequest.ReportId);
            }
            return true;
        }

        public void MarkIsActive(bool isActive, string userId, int reportId)
        {

            this.repository.MarkIsActive(isActive, userId, reportId);
            this.repository.CommitChanges();
        }

        private User GetUser(string userId)
        {
            return repository.GetUser(userId);
        }

        private Task<List<Report>> GetReports()
        {
            return repository.GetReports(x => x.IsActive).ToListAsync<Report>();
        }

        private Task<List<ReportGroupMapping>> GetReportGroupMapping()
        {
            return this.repository.GetReportGroupMapping(x => x.IsActive).ToListAsync<ReportGroupMapping>();
        }

        private Task<List<SecurityGroup>> GetSubGroups(int groupID)
        {
            return this.repository.GetSubGroups(x => x.IsActive && x.ParentGroupId == groupID).ToListAsync<SecurityGroup>();
        }
    }
}
