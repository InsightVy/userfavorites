﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.UserFavorites.Service.Models
{

    public class UserFavoritesGetRequestModel
    {
        public string UserId { get; set; }
        public int GroupId { get; set; }
    }

    public class UserFavoritesRequestModel
    {
        public string UserId { get; set; }
        public int ReportId { get; set; }
    }
}
