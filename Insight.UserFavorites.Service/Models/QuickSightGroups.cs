﻿using Insight.UserFavorites.SqlEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Insight.UserFavorites.Service.Models
{
    public class QuickSightGroups
    {
        [Key]
        public string qkgroupspk_id { get; set; }
        public string group_id { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string group_name { get; set; }
        public DateTime created_at { get; set; }
        public string updated_by { get; set; }
        public string s3bucketname { get; set; }
        public string s3objectkey { get; set; }
        public string s3iconurl { get; set; }
        public List<QuickSightDashboards> quicksightDashboards { get; set; }
    }
}
