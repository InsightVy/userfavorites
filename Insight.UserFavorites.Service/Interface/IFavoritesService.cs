﻿using Insight.UserFavorites.Service.Models;
using Insight.UserFavorites.SqlEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Insight.UserFavorites.Service
{
    public interface IUserFavoritesService
    {
        QuickSightGroups GetUserFavorites(UserFavoritesGetRequestModel request);
        void MarkIsActive(bool isActive, string userId, int reportId);
        bool AddUserFavorites(UserFavoritesRequestModel userFavorites);
        bool RemoveUserFavorites(UserFavoritesRequestModel userFavorites);
    }
}