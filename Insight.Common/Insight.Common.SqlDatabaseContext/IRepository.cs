﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Collections.Generic;

namespace Insight.Common.SqlDatabaseContext
{
    public interface IRepository
    {
        void Add<T>(T entity) where T : BaseEntity;
        void AddRange<T>(IEnumerable<T> entities) where T : BaseEntity;
        void RemoveRange<T>(IEnumerable<T> entities) where T : BaseEntity;
        void Update<T>(params T[] entities) where T : BaseEntity;
        void CommitChanges();

        void UseTransaction(IDbContextTransaction transaction);

        IDbContextTransaction StartTransaction();

        void CommitTransaction();
    }
}