﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Insight.Common.SqlDatabaseContext
{
    public static class DbHelper
    {
        public static void SetCreateFields(this BaseEntity entity, string userId)
        {
            entity.IsActive = true;
            entity.CreatedDateTime = DateTime.UtcNow;
            entity.CreatedBy = userId;
        }

        public static void SetUpdatedFields(this BaseEntity entity, string userId)
        {
            entity.UpdatedDateTime = DateTime.UtcNow;
            entity.UpdatedBy = userId;
        }
    }
}
