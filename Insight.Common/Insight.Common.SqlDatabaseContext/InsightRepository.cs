﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Collections.Generic;

namespace Insight.Common.SqlDatabaseContext
{
    public class InsightRepository : IRepository
    {
        private readonly InsightDbContext InsightDbContext;

        public InsightRepository(InsightDbContext insightDbContext)
        {
            this.InsightDbContext = insightDbContext;
        }

        public void UseTransaction(IDbContextTransaction transaction)
        {
            this.InsightDbContext.Database.UseTransaction(transaction.GetDbTransaction());
        }

        public IDbContextTransaction StartTransaction()
        {
            return this.InsightDbContext.Database.BeginTransaction();
        }
        public void CommitTransaction()
        {
             this.InsightDbContext.Database.CommitTransaction();
        }
        public void Add<T>(T entity) where T : BaseEntity
        {
            entity.SetCreateFields("Admin");
            this.InsightDbContext.Add(entity);
        }
        public void AddRange<T>(IEnumerable<T> entities) where T : BaseEntity
        {
            if (entities != null)
            {
                foreach (var entity in entities)
                {
                    entity.SetCreateFields("Admin");
                }
                this.InsightDbContext.AddRange(entities);
            }
        }
        public void RemoveRange<T>(IEnumerable<T> entities) where T : BaseEntity
        {
            if (entities != null)
            {
                this.InsightDbContext.RemoveRange(entities);
            }
        }
        public void Update<T>(params T[] entities) where T : BaseEntity
        {
            if (entities != null)
            {
                foreach (var entity in entities)
                {
                    entity.SetUpdatedFields("Admin");
                }
                this.InsightDbContext.UpdateRange(entities);
            }
        }

        public void CommitChanges()
        {
            this.InsightDbContext.SaveChanges();
        }

    }
}
