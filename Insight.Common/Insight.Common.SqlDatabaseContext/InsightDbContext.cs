﻿using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;

namespace Insight.Common.SqlDatabaseContext
{
    public partial class InsightDbContext : DbContext
    {
        private readonly IDbConnection connection;

        public InsightDbContext(IDbConnection connection)
        {
            this.connection = connection;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder
        .UseMySQL((DbConnection)connection);



    }

}
