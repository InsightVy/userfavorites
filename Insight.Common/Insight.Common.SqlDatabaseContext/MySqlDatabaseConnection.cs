﻿namespace Insight.Common.SqlDatabaseContext
{

    public class MySqlDatabaseConnection
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string DbInstanceIdentifier { get; set; }

        public string Dbname { get; set; }

        public override string ToString()
        {
            return @"server=" + Host + ";user id=" + UserName + ";password=" + Password + ";port=" + Port + ";database=" + (!string.IsNullOrEmpty(Dbname) ? Dbname : DbInstanceIdentifier) + ";";
        }
    }

}
