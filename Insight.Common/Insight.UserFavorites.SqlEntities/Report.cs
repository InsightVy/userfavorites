﻿using Insight.Common.SqlDatabaseContext;
using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.UserFavorites.SqlEntities
{
    public class Report : BaseEntity
    {
        public string ExternalReportId { get; set; }
        public string ReportName { get; set; }

        public string ReportDisplayName { get; set; }

        public string ReportDescription { get; set; }
        public string ReportOwner { get; set; }
        public string ReportOwnerEmail { get; set; }
        public string ReportPlatform { get; set; }
        public string ReportUrl { get; set; }
        public string ReportSnapshotImageUrl { get; set; }
        public string ReportSampleUrl { get; set; }
        public DateTime? LastPublishedDate { get; set; }

    }
}
