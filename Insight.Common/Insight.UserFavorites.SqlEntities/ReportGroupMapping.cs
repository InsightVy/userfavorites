﻿using Insight.Common.SqlDatabaseContext;
using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.UserFavorites.SqlEntities
{
    public class ReportGroupMapping : BaseEntity
    {
        public int ReportId { get; set; }
        public int GroupId { get; set; }
        public string ReportPlatform { get; set; }
    }
}
