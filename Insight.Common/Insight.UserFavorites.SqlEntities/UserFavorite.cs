﻿using Insight.Common.SqlDatabaseContext;
using System;

namespace Insight.UserFavorites.SqlEntities
{
    public class UserFavorite : BaseEntity
    {
        public int ReportId { get; set; }
        public string UserId { get; set; }
    }
}
