﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Insight.UserFavorites.SqlEntities
{
    public class User
    {
        [Key]
        public string user_id { get; set; }
        public string user_name { get; set; }
        public DateTime created_at { get; set; }
        public string updated_by { get; set; }
    }
}
