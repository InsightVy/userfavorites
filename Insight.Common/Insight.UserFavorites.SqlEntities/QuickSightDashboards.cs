﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Insight.UserFavorites.SqlEntities
{
    public class QuickSightDashboards
    {
        [Key]
        public string dashboardpk_id { get; set; }
        public string dashboard_id { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string dashboard_name { get; set; }
        public string group_id { get; set; }
        public string qkgroupspk_id { get; set; }
        public string dashboard_url { get; set; }
        public string ReportPlatform { get; set; }
        public DateTime created_at { get; set; }
        public string updated_by { get; set; }
        public string dashboards3imagepath { get; set; }
        public string dashboards3filepath { get; set; }
        public string IsQuicksight { get; set; }
    }
}
