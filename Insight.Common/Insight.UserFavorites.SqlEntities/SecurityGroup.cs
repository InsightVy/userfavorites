﻿using Insight.Common.SqlDatabaseContext;
using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.UserFavorites.SqlEntities
{
    public class SecurityGroup : BaseEntity
    {
        public string ExternalGroupId { get; set; }
        public string GroupDisplayName { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public string ReportPlatform { get; set; }
        public string GroupIconUrl { get; set; }
        public int? ParentGroupId { get; set; }
        public int? LandingReportCatalogId { get; set; }
        public int? DisplayOrder { get; set; }
    }
}
