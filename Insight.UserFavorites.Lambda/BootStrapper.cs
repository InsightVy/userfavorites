using Vyaire.Integration.Lambda;
using Microsoft.Extensions.DependencyInjection;
using Insight.UserFavorites.Service;
using Insight.UserFavorites.DataAccess;
using Vyaire.Common;
using Vyaire.Aws.Integrations;
using Insight.Common.SqlDatabaseContext;
using System.Data;
using MySql.Data.MySqlClient;

namespace Insight.UserFavorites.Lambda
{
    public class BootStrapper
    {

        static BootStrapper()
        {
        }

        public static void Init()
        {
            Arch.SetLogger(new LambdaLoggerService());
            Arch.SetConfiguration(ConfigurationManager.Instance);

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            Arch.SetFactory(serviceProvider);
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            var mySqlConnection = AwsSecretManagerUtility.GetSecret<MySqlDatabaseConnection>(Arch.Configuration["AwsSecretIdForMySql"], Arch.Configuration["AwsSecretManagerRegion"]);
            serviceCollection.AddScoped<IDbConnection>(x => new MySqlConnection(mySqlConnection.ToString()));
            Arch.Logger.Log(mySqlConnection.ToString());
            serviceCollection.AddTransient<UserFavoritesDbContext>();
            serviceCollection.AddTransient<IUserFavoritesRepository, UserFavoritesRepository>();
            serviceCollection.AddTransient<IUserFavoritesService, UserFavoritesService>();
        }

    }
}