﻿using System.Collections.Generic;
using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;
using Insight.UserFavorites.Service;
using System.Text.Json;
using Insight.UserFavorites.Service.Models;
using Vyaire.Common;
using Insight.UserFavorites.SqlEntities;
using System;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace Insight.UserFavorites.Lambda
{
    public class UserFavoritesHandler
    {
        /// <summary>
        /// Default constructor that Lambda will invoke.
        /// </summary>
        public UserFavoritesHandler()
        {
            BootStrapper.Init();
        }

        /// <summary>
        /// A Lambda function to respond to HTTP Get methods from API Gateway
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>The API Gateway response.</returns>
        public APIGatewayProxyResponse GetUserFavorites(APIGatewayProxyRequest apiRequest, ILambdaContext context)
        {
            QuickSightGroups serviceResponse = new QuickSightGroups();
            try
            {
                var request = JsonSerializer.Deserialize<UserFavoritesGetRequestModel>(apiRequest.Body);
                LambdaLogger.Log($"Request JSON - {JsonSerializer.Serialize(request)}");
                if (request.UserId != null)
                {
                    Arch.Logger.Log("Inside method GetUserFavorites");
                    Arch.Logger.Log($"Request user - {request.UserId}");
                    serviceResponse = Arch.Resolve<IUserFavoritesService>().GetUserFavorites(request);
                }

                return new APIGatewayProxyResponse
                {
                    StatusCode = 200,
                    Body = JsonSerializer.Serialize(serviceResponse),
                    Headers = new Dictionary<string, string>() { { "Content-Type", "application/json" }, { "Access-Control-Allow-Origin", "*" } }
                };
            }
            catch (Exception ex)
            {
                Arch.Logger.Log("Operation failed, method GetUserFavorites() : Message" + ex.Message);
                Arch.Logger.Log("Operation failed, method GetUserFavorites() : Inner Exception" + ex.InnerException);
                Arch.Logger.Log("Operation failed, method GetUserFavorites() : Source" + ex.Source);
                return new APIGatewayProxyResponse
                {
                    StatusCode = 500,
                    Body = JsonSerializer.Serialize(new { IsSuccess = false })
                };
            }
        }

        public APIGatewayProxyResponse AddUserFavorites(APIGatewayProxyRequest apiRequest, ILambdaContext context)
        {
            
            bool serviceResponse = false;
            try
            {
                var request = JsonSerializer.Deserialize<UserFavoritesRequestModel>(apiRequest.Body);
                LambdaLogger.Log($"Request JSON - {JsonSerializer.Serialize(request)}");
                if (request.UserId != null)
                {
                    Arch.Logger.Log("Inside method AddUserFavorites");
                    serviceResponse = Arch.Resolve<IUserFavoritesService>().AddUserFavorites(request);
                    Arch.Logger.Log("End of method AddUserFavorites");
                }
                return new APIGatewayProxyResponse
                {
                    StatusCode = 200,
                    Body = JsonSerializer.Serialize(serviceResponse),
                    Headers = new Dictionary<string, string>() { { "Content-Type", "application/json" }, { "Access-Control-Allow-Origin", "*" } }
                };
            }
            catch (Exception ex)
            {
                Arch.Logger.Log("Operation failed, method AddUserFavorites() : Message" + ex.Message);
                Arch.Logger.Log("Operation failed, method AddUserFavorites() : Inner Exception" + ex.InnerException);
                Arch.Logger.Log("Operation failed, method AddUserFavorites() : Source" + ex.Source);
                return new APIGatewayProxyResponse
                {
                    StatusCode = 500,
                    Body = JsonSerializer.Serialize(new { IsSuccess = false })
                };
            }
        }

        public APIGatewayProxyResponse RemoveUserFavorites(APIGatewayProxyRequest apiRequest, ILambdaContext context)
        {
            bool serviceResponse = false;
            try
            {
                var request = JsonSerializer.Deserialize<UserFavoritesRequestModel>(apiRequest.Body);
                LambdaLogger.Log($"Request JSON - {JsonSerializer.Serialize(request)}");
                if (request.UserId != null)
                {
                    Arch.Logger.Log("Inside method RemoveUserFavorites");
                    serviceResponse = Arch.Resolve<IUserFavoritesService>().RemoveUserFavorites(request);
                    Arch.Logger.Log("End of method RemoveUserFavorites");
                }
                return new APIGatewayProxyResponse
                {
                    StatusCode = 200,
                    Body = JsonSerializer.Serialize(serviceResponse),
                    Headers = new Dictionary<string, string>() { { "Content-Type", "application/json" }, { "Access-Control-Allow-Origin", "*" } }
                };
            }
            catch (Exception ex)
            {
                Arch.Logger.Log("Operation failed, method RemoveUserFavorites() : Message" + ex.Message);
                Arch.Logger.Log("Operation failed, method RemoveUserFavorites() : Inner Exception" + ex.InnerException);
                Arch.Logger.Log("Operation failed, method RemoveUserFavorites() : Source" + ex.Source);
                return new APIGatewayProxyResponse
                {
                    StatusCode = 500,
                    Body = JsonSerializer.Serialize(new { IsSuccess = false })
                };
            }
        }
    }

}
